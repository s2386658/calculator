package calculator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Calculator extends HttpServlet {
	private Calculating calculating;
	public void init() throws ServletException{
		this.calculating = new Calculating();
	}
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String docType =
				"<!DOCTYPE HTML>\n";
		String title = "Calculator";
		out.println(docType +
				"<HTML>\n" +
				"<HEAD><TITLE>" + title + "</TITLE>" +
				"<LINK REL=STYLESHEET HREF=\"styles.css\">" +
				"</HEAD>\n" +
				"<BODY BGCOLOR=\"#FDF5E6\">\n" +
				"<H1>" + title + "</H1>\n" +
				"  <P>Celsius number: " +
				request.getParameter("temp") + "\n" +
				"  <P>Price: " +
				Double.toString(this.calculating.getFahrenheit(request.getParameter("temp"))) +
				"</BODY></HTML>");

	}
}
