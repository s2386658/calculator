package calculator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatingTest {

	@Test
	void getFahrenheit() {
		Calculating calculating = new Calculating();
		double temp = calculating.getFahrenheit("1");
		Assertions.assertEquals(33.8,temp, 0.0,"temp of 1");
		temp = calculating.getFahrenheit("2");
		Assertions.assertEquals(35.6,temp, 0.0,"temp of 2");
		temp = calculating.getFahrenheit("3");
		Assertions.assertEquals(37.4,temp, 0.0,"temp of 3");
		temp = calculating.getFahrenheit("4");
		Assertions.assertEquals(39.2,temp, 0.0,"temp of 4");
		temp = calculating.getFahrenheit("5");
		Assertions.assertEquals(41.0,temp, 0.0,"temp of 5");
		temp = calculating.getFahrenheit("6");
		Assertions.assertEquals(42.8,temp, 0.0,"temp of 6");


	}
}